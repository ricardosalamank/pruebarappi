<?php

/**
 * Created by IntelliJ IDEA.
 * User: rsalamanca
 * Date: 2/11/17
 * Time: 02:16 PM
 */
class PostConfirmCtrRefactor
{
    function __construct()
    {

    }

    public function postConfirm()
    {
        $id = Input::get('service_id');
        $servicio = Service::find($id);

        if ($servicio != null) {
            if ($servicio->statusId == '6') {
                return Response::json(array('error' => '2'));
            }
            if ($servicio->driverId == null && $servicio->statusId == '1') {
                Service::update(
                    $id, array(
                        'driver_id' => Input::get('driver_id'),
                        'status_id' => '2'
                    )
                );
                Driver::update(
                    Input::get('driver_id'), array(
                        "available" => '0'
                    )
                );
                $driverTmp = Driver::find(Input::get('driver_id'));
                Service::update(
                    $id, array(
                        'car_id' => $driverTmp->carId
                    )
                );
//Notificar a usuario!!
                $pushMessage = 'Tu servicio ha sido confirmado!';

                $push = Push::make();
                if ($servicio->user->uuid == '') {
                    return Response::json(array('error' => '0'));
                }
                if ($servicio->user->type == '1') {//1Phone
                    $result = $push->ios(
                        $servicio->user->uuid, $pushMessage, 1, 'honk.wav',
                        'Open',
                        array('serviceId' => $servicio->id)
                    );
                } else {
                    $result = $push->android2(
                        $servicio->user->uuid, $pushMessage, 1, 'default',
                        'Open',
                        array('serviceId' => $servicio->id)
                    );
                }
                return Response::json(array('error' => '0', 'result' => $result));
            } else {
                return Response::json(array('error' => '1'));
            }
        } else {
            return Response::json(array('error' => '3'));
        }
    }

}